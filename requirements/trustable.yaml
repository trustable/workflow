kind: requirement
title: Trustable software requirements
description: |

    ## Nomenclature:
    - A "change" is any change to standards, requirements, tests, code,
    documentation, or other materials that affect the project, specifically
    including the initial upload of any component of any materials.

    - A "build" consists of any compilation, reformation, rearrangement,
    or copying of any sources to generate any artefacts that may become
    subject to test or deployment.

trustable-provenance:
    kind: requirement
    title: We know where it comes from
    description: |

        - Any upload of any change must be certain to belong to some uploader.
            - This implies an authentication mechanism in the patch tracker.
            - This implies a mechanism to override SCM provenance information
                - (Alice may assert Bob wrote a patch, but it was uploaded to the tracker by Alice, so the system must know this).
            - Any upload of any change must have a facility for attestation of origin
                - This implies metadata about changes beyond commit messages
            - No import of foreign sources is without attestation of origin
            - The SCM must have a gated branch with continuous history

trustable-build-automation:
    kind: requirement
    title: We know how to build it
    description: |
        - Any build must be performed by automation.
              - This implies the build instructions must be machine readable
              - This implies the build environment is produced by automation.
        - We have a complete log, including environment details, of at least two apparently identical successful builds.

trustable-build-reproducibility:
    kind: requirement
    title: We can reproduce it
    description: |
        - Any build performed twice must generate the same results.
               - This implies the sources must allow reproducible build
               - This implies the build environment must be isolated
        - Any deployment performed must be performed by automation
               - This implies the deployment instructions must be machine readable
               - This implies that some trustable software does not have identical state after deployment.

trustable-functionality:
    kind: requirement
    title: It does what it is supposed to do
    description: |
        - Any requirement must be expressed in a manner that can be verified
               - This implies the requirements are machine readable
               - This implies tooling to generate tests based on the requirements
               - The result of any test is machine readable
        - Any software deployed to production has prior evidence of passing tests
               - This implies pre-merge functionality in CI or similar
        - Any change must be associated with a requirement
        - There must exist information indicating the minimum tests required to gate
              - These tests must encompass all requirements related to the subject of the change

    kind: requirement
    title: It does not do what it is not supposed to do
    description: |
        FIXME: how do we formalize this?

trustable-updates:
    kind: requirement
    title: We can update it and be confident it will not break or regress
    description: |
        - Any update must undergo functional testing
               - This implies pre-merge integration and testing.
               - This implies stable build and deployment facilities
