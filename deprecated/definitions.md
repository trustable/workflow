# Definitions for t.software

## t.software

- t.software is composed of one or more t.changes
- we assess the trustability of t.software by analysing t.metrics
- t.software MUST have t.intents and/or t.requirements
- t.software MUST satisfy all of its t.intents and t.requirements
- there MUST be measurable t.evidence that t.software satisfies all of its
  t.intents and t.requirements

## t.change

- a t.change is any change to t.intents, t.requirements, t.features, t.tests,
 t.sourcecode, t.build or other materials that affect t.software
- a t.change specifically includes the initial upload of any component of any materials
- a t.change MUST be uniquely identifiable

## t.intent

- a t.intent is a business-level and/or stakeholder-level statement of
 needs or requirements
- t.intents are expressed in words/terms normally used by the business/stakeholder
- t.intents provide the base justification for all resulting t.requirements and
 other t.changes

## t.requirement

- each t.requirement MUST be associated with one or more t.intents
- each t.requirement MUST be clear/unambiguous
- each t.requirement MUST be verifiable/confirmable
- each t.requirement MUST be unitary/cohesive
- each t.requirement MUST be traceable from need to implementation via t.evidence
- each t.requirement MUST be correct/valid/true
- each t.requirement MAY be graded by priority/importance
- the set of t.requirements MUST be complete
  - known omissions and assumptions MUST be stated
- the set of t.requirements MUST be consistent/coherent
- t.requirements MAY include standards
- all t.requirements MUST have t.tests

## t.feature

- each t.feature MUST be associated with one or more (t.requirement or t.intent)
- each t.feature MUST be validated by one or more t.test

## t.test

- each t.test MUST be associated with one or more t.requirement
- all t.tests SHOULD be capable of automated execution
- all t.tests MUST be executed and pass
- the execution of t.tests MUST produce t.evidence

## t.evidence

- t.evidence is machine-parsable metadata associated with t.software
- t.evidence MUST be recorded in a manner confirming its own integrity/correctness
- t.evidence MUST identify its t.contributor
- t.evidence MUST include creation/modification/commit time-and-date
- t.evidence MAY include text description and/or comments
- t.evidence MUST identify the t.contributors of its t.changes
- t.evidence MUST identify the t.contributors that review its t.changes
- t.evidence MUST be produced for each:
  - submission of a t.change
  - review of a t.change
  - construction of a t.environment
  - construction of a t.artefact
  - deployment of a t.artefact
  - validation of a t.change
- all t.evidence MUST be uniquely identifiable

## t.contributor

- a t.contributor is a uniquely identifiable person, organisation or software program
- t.contributors contribute to t.software and/or t.evidence
- t.contributors are responsible and accountable for their contributions
- a t.contributor MUST have a unique identity and be authorised to contribute to
 t.software
- a t.contributor MUST authenticate to confirm identity in order to contribute to
 t.software

## t.estimate

- t.estimates MUST be derived from t.evidence via automation
- t.estimates are known to be uncertain
- we MAY use t.estimates to fill in gaps in t.metrics
- we MAY iteratively improve the t.estimate model by analysing t.metrics
- we MAY produce a t.estimate of the scale or scope of missing t.requirements
- we MAY produce a t.estimate of the scale or scope of missing t.tests

## t.metric

- t.metrics MUST be derived from t.evidence via automation
- t.metrics MUST be believed to be factually correct
- t.metrics MAY include any of the following example items:
  - the t.change history for each t.feature
  - the number of t.tests that exist for each t.feature
  - the number of t.tests that have passed for each t.feature
  - the t.requirements implemented by each t.feature
  - the mean time taken to construct each t.artifact
  - the number of t.environments constructed to validate each t.feature
  - the time taken to complete each t.feature
- we look for conflicts between t.metrics
  - we evaluate the conflicts
- we aim to identify gaps in t.metrics
  - we seek new t.evidence to close them
- all t.metrics MUST be meaningful
  - for t.software, all t.metrics are acceptable
- we can aggregate/manipulate t.metrics to provide a measure of trustability

## t.build

- a t.build is the information necessary for the compilation, reformation,
 rearrangement, or copying of any t.changes to generate t.artefacts, t.environments
 and/or t.software
- the t.build MUST define any external artefacts (t.artefact) on which the
 t.artefact, t.environment or t.software is dependent

## t.sourcecode

- t.sourcecode is any t.change that may be compiled, reformed, rearranged, or
 copied to generate t.artefacts, t.environments and/or t.software

## t.artefact

- a t.artefact is any output from software construction which is retained to be
 used as input to another software construction or as part of a release of t.software
- a t.artefact MUST have t.evidence of the t.sourcecode and t.build used to
 construct it
- any two constructions of t.artefact using the same t.build and t.software
 should result in bit-for-bit equivalent t.artefacts
- any such two constructions MUST be able to be separated in time, or be
 performed in different (but behaviourally equivalent) t.environments
- a t.artefact MUST be uniquely identifiable

## t.environment

- a t.environment is a set of resources (hardware and software) onto which
 t.artefacts can be deployed and executed
- a t.environment MUST be reproducible (behaviourally equivalent)

## t.process, t.effect, t.friction, t.value

- t.process is a set of practices to increase the trustability of software
- adding t.process to a project MUST lead to measurable t.effects
- t.effects are t.metrics
- t.friction is negative t.effects
- t.value is positive t.effects
- adding t.process to a mature (widely trusted) project must
  - provide measurable t.value
  - involve acceptable (minimal) t.friction
  - be justifiable to be used upstream and/or
  - be applicable downstream

## t.release

- a t.release is an aggregation of t.artefacts
- a t.release MUST be uniquely identifiable
- a t.release is capable of deployment to production t.environments
- t.software exists in a world where changes to t.requirements will occur after
 any given release of the t.software, therefore all t.software MUST be capable of
 in-field update
- t.release MAY refer to all t.software on a system, or to any well-defined
 subset thereof
- a t.release MUST be atomically applicable with rollback capability
- a t.release MUST be cryptographically validatable prior to t.software
 consuming it.
- FIXME add hypothesis update logic here

## t.context

- t.context is written information which states or clarifies relevant ideas for t.software
- [trustable software discussions](https://lists.trustable.io/pipermail/trustable-software/)
- [wikipedia requirement](https://en.wikipedia.org/wiki/Requirement)
- [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt)
- virtually all existing software does not satisfy the [trustable software hypothesis](https://gitlab.com/trustable/overview/wikis/hypothesis-for-software-to-be-trustable)
  - but a lot of software is widely trusted
- to be useful, the t.software concepts must be
  - easy to understand
  - justifiable
  - practical and realistic
  - applicable to existing software
    - collecting t.evidence needs to be low friction
    - t.metrics for widely trusted existing software should be high
