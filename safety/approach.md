# WIP trustable:safety approach

FIXME: aim towards worked example based on minimal distro

## STPA

Systems-Theoretic Process Analysis is a System Engineering approach to
analysing and modelling safety for complex systems including regulatory and
cultural interactions as well as engineered systems containing electronics and
software.

STPA appears to be applicable for Trustable Software because it provides a
framework that can be applied to software and systems at all scales (from
microcontrollers, to vehicles, to industrial plant, to cloud).

- STPA starts from relevant losses and leads to a definition of
  - safety constraints
  - hierarchical control structures
    - one for design, development, manufacture
	- one for operation
  - process models


```
     +---------------------+
     |           +-------+ |
     |  control  |process| |
     | algorithm | model | |
     |           +-------+ |
     +--+---------------+--+
        |               |
control v               ^ feedback
actions |               |
        |               |
     +--+---------------+--+
     |                     |
     | controlled process  |
     |                     |
     +---------------------+
```

We start by identifying the losses that we aim to avoid.

Then we build up the analysis model as a set of control structures which
describe how components (including non-technical components, e.g. users,
legislation) interact to cause and/or prevent these losses.

Then we can use the model to assess the safety of proposed or actual designs.

### Miscellaneous
For specific cases where reliability/determinism matters
- static code analysis
- dynamic code analysis
- code coverage?

- what other options do we have?
